console.log("Bai 1: ");
const paymentPerDay = 100000;
var day = 6;
console.log("So ngay lam viec: ", day);
console.log("Salary: ", paymentPerDay * day);


console.log("Bai 2: ");
var a = 1;
var b = 2;
var c = 3;
var d = 4;
var e = 5;
console.log("Average value: ", (a + b + c + d + e) / 5);


console.log("Bai 3: ");
var USD = 8;
console.log("USD: ", USD);
console.log(USD + " USD quy doi ra VND la: ", USD * 23500);


console.log("Bai 4: ");
var long = 10;
var width = 5;
console.log("Chieu dai cua HCN la: ", long);
console.log("Chieu rong cua HCN la: ", width);
console.log("Dien tich cua HCN la: ", long * width);
console.log("Chu vi cua HCN la: ", (long + width) * 2);


console.log("Bai 5: ");
var number = 26;
var units = number % 10;
var dozens = (number - units) / 10;
console.log("Number: ", number);
console.log("Tong 2 ky so la: ", dozens + units);